package cn.torna.web.controller.module.vo;

import lombok.Data;

import java.util.List;

/**
 * @author tanghc
 */
@Data
public class SwaggerSettingVO {
    private String allowMethod;

}
